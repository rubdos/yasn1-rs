extern crate asn1;
extern crate serde;
#[macro_use]
extern crate serde_derive;

use asn1::prelude::*;

#[derive(Serialize)]
pub struct TbsCertificate;

#[derive(Serialize)]
pub struct X509Certificate {
    tbs_certificate: TbsCertificate,
    signature_algorithm: Oid,
}

fn main() {
    // TODO: stub
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_simple_certificate() {
    }
}
