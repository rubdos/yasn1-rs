#![feature(specialization)]

extern crate asn1;
extern crate serde;

#[cfg(any(test, feature="serde_derive"))]
#[macro_use]
extern crate serde_derive;

mod error;
mod ser;

pub use ser::{Serializer,to_asn1};
