# [Serde][serde.rs] compatible ASN.1 library for [Rust][rust-lang]

[![build status](https://gitlab.com/glycos/yasn1-rs/badges/master/build.svg)](https://gitlab.com/glycos/yasn1-rs/commits/master)
[![coverage report](https://gitlab.com/glycos/yasn1-rs/badges/master/coverage.svg)](https://glycos.gitlab.io/yasn1-rs/coverage/)
[![Crates.io](https://img.shields.io/crates/v/serde_asn1.svg)](https://crates.io/crates/serde_asn1)


(WIP) ASN.1 BER parsing, DER emitting library for the [Rust programming language][rust-lang].
Currently, [Serde][serde.rs] compatibilty depends on a nightly Rust version (we use [specialization](https://github.com/rust-lang/rust/issues/31844)).

## Features:

- [x] DER serialization:
  - [x] Boolean(`bool`),
  - [x] Integer(`i64`),
  - [x] BigInteger(`num_bigint::BigInt`),
  - [ ] BitString,
  - [ ] OctetString(`Vec<u8>`),
  - [ ] Null,
  - [x] OID(`types::Oid`),
  - [ ] Real(`f64`),
  - [ ] Enum,
  - [x] Sequence(`Vec<Asn1>`, `struct`s, ...),
  - [x] UTCTime(`chrono::DateTime<chrono::Utc>`),
  - [x] GeneralizedTime(`chrono::DateTime<chrono::Utc>`),
- [ ] BER deserialization:
  - [ ] Boolean(`bool`),
  - [ ] Integer(`i64`),
  - [ ] BigInteger(`num_bigint::BigInt`),
  - [ ] BitString,
  - [ ] OctetString(`Vec<u8>`),
  - [ ] Null,
  - [ ] OID(`types::Oid`),
  - [ ] Real(`f64`),
  - [ ] Enum,
  - [ ] Sequence(`Vec<Asn1>`, `struct`s, ...),
  - [ ] UTCTime(`chrono::DateTime<chrono::Utc>`),
  - [ ] GeneralizedTime(`chrono::DateTime<chrono::Utc>`),

[rust-lang]: http://rust-lang.org/
[serde.rs]: https://serde.rs
