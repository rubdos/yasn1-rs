use std;

mod se;

#[derive(Debug)]
pub struct Error;
pub type Result<T> = std::result::Result<T, Error>;

pub trait Der {
    fn der_encode<'a>(self) -> Result<Vec<u8>>;
}
