use ::Asn1;

use ::der::*;

struct DerEncoder {
    buf: Vec<u8>,
}

#[repr(u8)]
enum Tag {
    Boolean = 0x01,
    Integer = 0x02,
    BitString = 0x03,
    OctetString = 0x04,
    Null = 0x05,
    OID = 0x06,
    Real = 0x09,
    Enum = 0x0A,
    Sequence = 0x30,
    UTCTime = 0x17,
    GeneralizedTime = 0x18,
}

impl Tag {
    fn from_asn1(asn: &Asn1) -> Self {
        match asn {
            &Asn1::Boolean(_) => Tag::Boolean,
            &Asn1::Integer(_) => Tag::Integer,
            &Asn1::BigInteger(_) => Tag::Integer,
            &Asn1::BitString => Tag::BitString,
            &Asn1::OctetString(_) => Tag::OctetString,
            &Asn1::Null => Tag::Null,
            &Asn1::OID(_) => Tag::OID,
            &Asn1::Real(_) => Tag::Real,
            &Asn1::Enum(_) => Tag::Enum,
            &Asn1::Sequence(_) => Tag::Sequence,
            &Asn1::UTCTime(_) => Tag::UTCTime,
            &Asn1::GeneralizedTime(_) => Tag::GeneralizedTime,
        }
    }
}

impl Der for ::Asn1 {
    fn der_encode<'a>(self) -> Result<Vec<u8>> {
        let encoder = DerEncoder::new();
        let encoder = encoder.encode(self)?;
        Ok(encoder.to_bytes())
    }
}

impl DerEncoder {
    fn new() -> DerEncoder {
        DerEncoder {
            buf: Vec::new(),
        }
    }

    fn encode(mut self, asn: Asn1) -> Result<DerEncoder> {
        self.buf.push(Tag::from_asn1(&asn) as u8);
        match asn {
            Asn1::Boolean(value) => {
                self.buf.push(1); // length
                if value {
                    self.buf.push(1);
                } else {
                    self.buf.push(0);
                }
            },
            Asn1::Integer(int) => {
                let mut shift = 56;
                while shift > 0 && (int >> (shift - 1) == 0 || int >> (shift - 1) == -1) {
                    shift -= 8;
                }
                self.write_len(shift / 8 + 1);
                loop {
                    self.buf.push((int >> shift) as u8);
                    if shift == 0 {
                        break;
                    }
                    shift -= 8;
                }
            },
            Asn1::BigInteger(int) => {
                let mut bytes = int.to_signed_bytes_be();
                self.write_len(bytes.len());
                self.buf.append(&mut bytes);
            },
            Asn1::Sequence(s) => {
                let before = self.buf.len();
                for el in s {
                    self = self.encode(el)?;
                }
                let seq_len = self.buf.len() - before;
                if seq_len > 127 {
                    return Err(Error);
                }
                self.buf.insert(before, seq_len as u8);
            },
            Asn1::UTCTime(time) => {
                let time = format!("{}", time.format("%Y%m%d%H%M%SZ"));
                self.write_len(time.len());
                self.buf.append(&mut time.into_bytes());
            }
            Asn1::GeneralizedTime(time) => {
                // FIXME: are there implementations of ASN.1/DER that we can verify to implement
                // sub-second precision?
                let mut time = format!("{}", time.format("%Y%m%d%H%M%S%f"));
                let (prefix, fraction) = time.split_at_mut(14);
                #[cfg(nightly)]
                let r = fraction.as_bytes().iter().enumerate().rfind(|&(_, &x)| x != b'0');
                #[cfg(not(nightly))]
                let r = fraction.as_bytes().iter().enumerate().rev().find(|&(_, &x)| x != b'0');
                let time = match r {
                    Some((offset, _)) => prefix.to_owned() + "." + &fraction[0..offset+1] + "Z",
                    None => prefix.to_owned() + "Z",
                };
                self.write_len(time.len());
                self.buf.append(&mut time.into_bytes());
            },
            Asn1::OID(oid) => {
                let mut bytes = oid.x690_encode();
                self.write_len(bytes.len());
                self.buf.append(&mut bytes);
            },
            _ => return Err(Error),
        };
        Ok(self)
    }

    fn write_len(&mut self, len: usize) {
        self.buf.push(len as u8);
    }

    fn to_bytes(self) -> Vec<u8> {
        self.buf
    }
}

#[cfg(test)]
mod tests {
    use ::Asn1;
    use ::der::Der;

    #[test]
    fn test_simple() {
        let val = Asn1::Sequence(vec![
            Asn1::Boolean(true),
            Asn1::Integer(49),
        ]);

        let bytes = Der::der_encode(val).unwrap();
        assert_eq!(vec![0x30, 0x06, 0x01, 0x01, 0x01, 0x02, 0x01, 0x31], bytes);
    }

    #[test]
    fn test_bigint() {
        use num_bigint::BigInt;
        use num_traits::Num;

        let val = Asn1::Sequence(vec![
            Asn1::Boolean(true),
            Asn1::BigInteger(BigInt::from_str_radix("AAAAAAAAAAAAAAAABB", 16).unwrap()),
        ]);

        let bytes = Der::der_encode(val).unwrap();
        assert_eq!(vec![0x30, 0x0F, 0x01, 0x01, 0x01, 0x02, 0x0A, 0x00,
                   0xAA,0xAA, 0xAA, 0xAA, 0xAA,0xAA, 0xAA, 0xAA, 0xBB], bytes);

        let val = Asn1::BigInteger(0.into());
        let bytes = Der::der_encode(val).unwrap();
        assert_eq!(vec![0x02, 0x01, 0x00], bytes);
    }

    #[test]
    fn test_time() {
        use chrono;
        use chrono::prelude::*;
        let val = Asn1::GeneralizedTime(chrono::Utc.ymd(2014, 11, 14).and_hms(8, 9, 10));
        let bytes = Der::der_encode(val).unwrap();
        assert_eq!(vec![0x18, 15, 50, 48, 49, 52, 49, 49, 49, 52, 48, 56, 48, 57, 49, 48, 90], bytes);

        let val = Asn1::GeneralizedTime(chrono::Utc.ymd(2014, 11, 14).and_hms_nano(8, 9, 10, 123456000));
        let bytes = Der::der_encode(val).unwrap();
        for byte in &bytes {
            print!("{:02X}", byte);
        }
        println!();
        assert_eq!(vec![0x18, 22, 50, 48, 49, 52, 49, 49, 49, 52, 48, 56, 48, 57, 49, 48, 46, 49, 50, 51, 52, 53, 54, 90], bytes);
    }

    #[test]
    fn test_oid() {
        let vectors = vec![
            ("1.2", vec![0x06, 0x01, 0x2A]),
            ("1.2.840", vec![6, 3, 42, 134, 72]),
            ("1.2.840.113549.1", vec![6, 7, 42, 134, 72, 134, 247, 13, 1]),
        ];

        for (s, v) in vectors {
            let asn1 = Asn1::OID(s.parse::<::types::Oid>().unwrap());
            let bytes = asn1.der_encode().unwrap();
            for byte in &bytes {
                print!("{:02X}", byte);
            }
            println!();
            assert_eq!(v, bytes);
        }
    }
}
