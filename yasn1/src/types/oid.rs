use std;
use std::str::FromStr;
use std::num::ParseIntError;
use std::fmt::{Debug, Display, Formatter};

#[cfg(feature = "serde")]
use serde::{Serialize,Serializer};

#[derive(Clone)]
pub struct Oid {
    primary_components: u8,
    optional_components: Vec<u64>,
}

impl Oid {
    pub fn create(v1: u8, v2: u8, rest: Vec<u64>) -> Oid {
        Oid {
            primary_components: v1*40 + v2,
            optional_components: rest,
        }
    }
    pub fn index(&self, index: usize) -> u64 {
        match index {
            0 => self.primary_components as u64 / 40,
            1 => self.primary_components as u64 % 40,
            index => self.optional_components[index - 2],
        }
    }
    pub fn x690_encode(self) -> Vec<u8> {
        let mut buf = Vec::with_capacity(1 + self.optional_components.len() * 8);
        buf.push(self.primary_components);

        // base 128 big endian encoding... Yeh.

        for mut component in self.optional_components {
            if component == 0 {
                buf.push(0);
                continue;
            }
            // Vector capacity is ceil(used_digits/8)
            let mut component_buffer = Vec::with_capacity((64 - component.leading_zeros() as usize + 7)/8);
            let mut first = true;
            while component != 0 {
                let mut v = (component % 128) as u8;
                component >>= 7;
                if !first {
                    v |= 0x80;
                }
                first = false;
                component_buffer.push(v);
            }
            buf.extend(&mut component_buffer.iter().rev());
        }

        buf
    }
}

impl Display for Oid {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "{}.{}", self.index(0), self.index(1))?;
        for e in self.optional_components.iter() {
            write!(f, ".{}", e)?;
        }
        Ok(())
    }
}

impl Debug for Oid {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "[Oid={}]", self)?;
        Ok(())
    }
}

#[derive(Debug)]
pub enum ParseOidError {
    NoFirstComponent,
    NoSecondComponent,
    ParseIntError(ParseIntError),
}

impl From<ParseIntError> for ParseOidError {
    fn from(i: ParseIntError) -> ParseOidError {
        ParseOidError::ParseIntError(i)
    }
}

impl FromStr for Oid {
    type Err = ParseOidError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut components = s.split('.');
        let v1 = components.next().ok_or(ParseOidError::NoFirstComponent)?.parse::<u8>()?;
        let v2 = components.next().ok_or(ParseOidError::NoSecondComponent)?.parse::<u8>()?;
        let rest = components.map(|a| a.parse::<u64>()).collect::<Result<_,_>>()?;
        Ok(Oid::create(v1, v2, rest))
    }
}

#[cfg(feature = "serde")]
pub trait OidSerializer {
    type Ok;
    type Error;
    fn serialize_oid(self, &Oid) -> Result<Self::Ok, Self::Error>;
}

#[cfg(feature = "serde")]
impl<T> OidSerializer for T
    where T: Serializer
{
    type Ok = T::Ok;
    type Error = T::Error;
    default fn serialize_oid(self, oid: &Oid) -> Result<Self::Ok, Self::Error> {
        self.serialize_str(&format!("{}", oid))
    }
}

#[cfg(feature = "serde")]
impl Serialize for Oid {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        serializer.serialize_oid(self)
    }
}

#[cfg(test)]
mod tests {
    use ::types::Oid;

    #[test]
    fn test_vectors() {
        let vectors = vec![
            ("1.2", 1, 2, vec![]), // ISO member bodies
            ("1.2.840", 1, 2, vec![840]), // US ANSI
            ("1.2.840.113549.1", 1, 2, vec![840, 113549, 1]), // RSA PKCS
        ];

        for (result, v1, v2, rest) in vectors {
            let value = Oid::create(v1, v2, rest);
            assert_eq!(result, format!("{}", value));
        }
    }

    #[test]
    fn test_parse() {
        let vectors = vec![
            "1.2",
            "1.2.840",
            "1.2.840.113549.1",
        ];

        for s in vectors {
            assert_eq!(s, format!("{}", s.parse::<Oid>().unwrap()));
        }
    }
}
