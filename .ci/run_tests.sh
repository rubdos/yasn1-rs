echo "Running on Rust $1"

set -e

(cd yasn1; cargo test)
(cd yasn1; cargo test --no-default-features)

[ "$1" == "NIGHTLY" ] || exit 0;

echo "Testing nightly-only features"

(cd yasn1; cargo test --features="serde")

(cd serde_asn1; cargo test --no-default-features)
(cd serde_asn1; cargo test)
