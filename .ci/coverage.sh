# Prepare for kcov

ASN1_EXE=(target/debug/asn1-*)
ASN1_EXE=${ASN1_EXE[0]}

SERDE_ASN1_EXE=(target/debug/serde_asn1-*)
SERDE_ASN1_EXE=${SERDE_ASN1_EXE[0]}

cp $ASN1_EXE target/debug/asn1-tests
cp $SERDE_ASN1_EXE target/debug/serde_asn1-tests

# Run kcov

mkdir -p public/coverage
kcov --exclude-pattern=.cargo --verify public/coverage/ target/debug/asn1-tests
kcov --exclude-pattern=.cargo --verify public/coverage/ target/debug/serde_asn1-tests

cat public/coverage/kcov-merged/coverage.json
